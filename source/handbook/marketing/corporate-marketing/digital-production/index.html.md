---
layout: markdown_page
title: "Digital production"
---

## SLAs for video production:

- Must make requests with no less than six weeks in advance for event videography.
- Must make requests with no less than eight weeks in advance for a customer or user story.
- Must have approval of all customers, users and other external partners that are to be videographed prior to making a production request.
- Must have approval from the event venue that videography and photography is permitted on the premises and have any and all required documentation and insurance from the site approved and provided at the time of request.
- Need all suggested questions for videos in two weeks before the shoot date.
- Contribute - no requests will be accepted on site
- Contribute - must use the production section for timeslot requests and be flexible bc some intvs can run long and impact the day's production schedule

