---
layout: markdown_page
title: "Category Vision - Interactive Application Security Testing (IAST)"
---

- TOC
{:toc}

## Description

<!-- A good description of what your category is. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Please include usecases, personas, 
and user journeys into this section. -->

Applications that are deployed to production are subject to attacks that spot vulnerabilities that cannot be found looking at the source code like SAST does.

Some of them can be detected by DAST, but in this case you just have an output if the vulnerability is creating an "external" feedback (like a `500` error). If the problem is visible only "inside" the application, there is no easy way to know it happened.

Interactive Application Security Testing (IAST) aims to see how an app reacts to a security scan from inside the application. This is normally done using an agent that is deployed as part of the application, and that is strictly integrated with it.

Runtime Application Self-Protection Security (RASP) uses this approach to spot vulnerabilities looking at the internal calls that may report errors while an external probe is running, like DAST does.

## Target audience and experience
<!-- An overview of the personas involved in this category. An overview 
of the evolving user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels.-->

## What's next & why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

## Maturity Plan
 - [Base Epic](https://gitlab.com/groups/gitlab-org/-/epics/344)
 
## Competitive landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

## Analyst landscape
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

## Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

## Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

## Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

## Top Vision Item(s)
<!-- What's the most important thing to move your vision forward?-->