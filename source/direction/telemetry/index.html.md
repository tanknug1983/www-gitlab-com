---
layout: markdown_page
title: Product Vision - Telemetry
---

## On this page
{:.no_toc}

- TOC
{:toc}

This is the product vision for Telemetry. Telemetry manages a variety of categories that are important for GitLab's understanding of how our users use our products.

If you'd like to discuss this vision directly with the Product Manager for Telemetry, feel free to reach out to Luca Williams via [e-mail](mailto:luca@gitlab.com), [Twitter](https://twitter.com/tipyn2903), or by [scheduling a video call](https://calendly.com/tipyn).

| Category | Description |
| ------ |  ------ |
| [🧺 Collection](/direction/telemetry/collection) | The structure(s) and platform(s) of how we collect Telemetry data |
| [🔍 Analysis](/direction/telemetry/analysis) | Manages GitLab's internal needs for an analysis tool that serves the Product department |

The overall vision for the Telemetry group is to ensure that we have a robust, consistent and modern telemetry data framework in place to best serve our internal Product, Sales and Customer Success teams. The group also maintains the [Version Check](https://docs.gitlab.com/ee/user/admin_area/settings/usage_statistics.html#version-check-core-only) application as described above and also ensures that GitLab's Product team have the right analysis tool in place that allows the best possible insights into the data provided through the Collection tools.

Improving how we collect and use product usage data is the first and most crucial stepping stone to visualising our data so we can make the best decisions for our users and therefore our business. We also want Telemetry data to be a gateway to providing great meta features to users. Here are some examples of these:

* License key synchronisation so you never have to worry about managing your license keys again
* The ability to raise support tickets effortlessly from within GitLab itself which includes a automatic snapshot of what happened in the last few minutes
* Access to a fair billing model by sending regular user activity data to GitLab's license servers

### 🤔 What is Telemetry, Anyway?  

[Telemetry](https://en.wikipedia.org/wiki/Telemetry) is an automated communications process by which measurements and other data are collected at remote or inaccessible points and transmitted to receiving equipment for monitoring. The word is derived from Greek roots: tele = remote, and metron = measure. In software, Telemetry data records clicks, counts, time spent on tasks, webpages, growth in the usage of a feature over time, trends of all these things, etc. 

Telemetry data is essential to building a successful product. It provides insights into how users interact with and use a product and allow many functions within a company to make educated decisions. To build an efficient, well-considered product with a great user experience, Product Managers need insights on how the decisions they previously made are impacting users. It allows them to understand what matters to users and what doesn't and helps them deliver high quality, low effort interactions that users love. ❤️ 

Telemetry is often viewed through a negative lens. Privacy is becoming more and more of a serious topic in recent years due to the rise in social media, [GDPR](https://en.wikipedia.org/wiki/General_Data_Protection_Regulation) and the increasing need for more secure applications to protect users. However, Telemetry is not just a term used in software, it is commonly used in medicine, science, meteorology and other similar areas. There is even such a job as a Telemetry Nurse, which is a specialised role in the medical field where Nurses monitor patients vitals, record and interpret the data and provide this to Doctors to show how a patient is recovering, allowing the Doctor to prescribe a more effective and care-driven treatment plan.

This is not so different from software, really. 

Telemetry data doesn't just make Product Managers lives easier; It's also hugely beneficial to Customer Success teams by allowing them to see where users may need additional help or support. It enables Customer Success Managers and Technical Account Managers to proactively reach out and make sure that users are happy with what they've purchased. It's equally beneficial to teams such as Product Marketing and Sales by helping them know what story to tell future users of the product and what might be useful to current users to keep them up to date and well informed.

### 🚀 Collection Priorities

#### 🔥 Current focus

| Priority | Focus | Why? |
| :------: | ------ | ------ |
| 1️⃣ | [Define and measure monthly active users, overall and per-stage](https://gitlab.com/groups/gitlab-org/-/epics/1374) | After this epic is closed, we should have an internally consistent view of MAU and SMAU across GitLab.com and self-managed. We should be able to measure active use in a Periscope dashboard, enabling us to improve MAU and SMAU. We can then tackle improving this further with [SMAU/MAU v2.0.](https://gitlab.com/groups/gitlab-org/-/epics/1440) |

#### 🎉 Next up

| Priority | Focus | Why? |
| :------: | ------ | ------ |
| 2️⃣ | [SMAU/MAU v2.0.](https://gitlab.com/groups/gitlab-org/-/epics/1440) | As our organization grows, we require better data to inform our product, marketing, and sales team as they make decisions to grow the business and realize our strategic goals. This epic will serve as the aggregation of issues required to improve our monthly active user metrics, so we can have a world class data platform at GitLab. |
| 3️⃣ | [Self-Managed Telemetry Tracking](https://gitlab.com/gitlab-org/telemetry/issues/34) | Currently we have little to no visibility into how many of our largest and most valuable customers are using GitLab. We need to understand how we can collect data more consistently from our Self-Managed users in order to better serve them. |
| 4️⃣ | [Ability to track all events on non-opted-out self-managed instances](https://gitlab.com/gitlab-org/telemetry/issues/58) | Now that we have more robust Telemetry data being collected on GitLab.com using [Snowplow](https://snowplowanalytics.com/), we should focus on implementing the same usage tracking for our opted-in Self-Managed instances. |

### 🚀 Analysis Priorities

### 🔥 Current focus

| Priority | Focus | Why? |
| :------: | ------ | ------ |
| 1️⃣ | [Manage Team Usage Analytics Dashboard](https://gitlab.com/gitlab-org/telemetry/issues/20) | Focusing on this issue means we can create a repeatable process for all the other stages listed in [this epic](https://gitlab.com/groups/gitlab-org/-/epics/1325). This epic will allow each stage to have a customizable dashboard which also measures SMAU/MAU - an important metric for GitLab as a business. The currently running [Telemetry Working Group](https://about.gitlab.com/company/team/structure/working-groups/telemetry/) has set the successful closure of this issue as the exit criteria. |

### 🎉 Next up

| Priority | Focus | Why? |
| :------: | ------ | ------ |
| 2️⃣ | [Product Management Periscope Training](https://gitlab.com/gitlab-org/telemetry/issues/13) | Once the Product Team are fully trained on the Analysis tool we have today ([Periscope](https://www.periscopedata.com/)), they will be empowered to ask and answer questions of the data we have available and that data will drive important decisions for the stage/business. |
| 3️⃣ | [Product Team Dashboards](https://gitlab.com/groups/gitlab-org/-/epics/1325) | Parallel to improving [SMAU/MAU v2.0.](https://gitlab.com/groups/gitlab-org/-/epics/1440), it's important that we roll out the process defined by the Telemetry working group to all of the other stages so that each Product Manager has visibility into how users are using their stage and stage categories. |


<!-- >## What's next

#### Immediate priorities

#### Next up -->

## How we prioritize

We follow the same prioritization guidelines as the [product team at large](https://about.gitlab.com/handbook/product/#prioritization). Issues tend to flow from having no milestone, to being added to the backlog, to a directional milestone (e.g. Next 3-4 releases), and are finally assigned a specific milestone.

Our entire public backlog for Telemetry can be viewed [here](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=group%3A%3Atelemetry&scope=all&sort=popularity&state=opened&utf8=%E2%9C%93), and can be filtered by labels or milestones. If you find something you are interested in, you're encouraged to jump into the conversation and participate. At GitLab, everyone can contribute!


